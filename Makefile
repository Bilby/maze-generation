# Alastair Bilby
# Standard Makefile

# Standard setup, linux:
CC=g++
TARGET_DIR=build/
TARGET=linux_build
CFLAGS=-Wall -Wextra -pedantic -std=c++11 -pthread -O2
LDFLAGS=-I lib/
OBJECTS=$(src/*.cpp)
HEADERS=$(src/*.h)
SRC=$(shell find src -type f -name '*.cpp')
OBJ=$(SRC:.cpp=.o)

# Windows:
ifeq "$(OS)" "Windows_NT"
TARGET=windows_build.exe
CFLAGS+=-WIN32

# Apple:
else ifeq ($(shell uname -s),Darwin)
TARGET=osx_build
CFLAGS+=-D __APPLE__
endif

all: $(TARGET)

.PHONY:
debug: CFLAGS-= -O2
debug: CFLAGS+= -g -DEBUG -O0 -DSF_VISIBILITY
debug: all

$(TARGET): $(OBJ) $(HEADERS)
	@echo Compilation Completed. Linking $(TARGET_DIR)$(TARGET)
	@$(CC) $(CFLAGS) $(OBJ) -o $(TARGET_DIR)$(TARGET) $(LDFLAGS)

%.o: %.cpp
	@echo Compiling: $@ $<
	@$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS)

.PHONY:
clean:
	@echo Cleaning target
	rm -f $(TARGET_DIR)$(TARGET)
	@echo Cleaning object files
	rm -f src/*.o
	rm -f src/*.o.

