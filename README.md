### Maze Generation Project

Used to see how long different maze generation algorithms take.

Currently working with Prim's, Kruskal's, recursive division and recursive backtracing.
