#include "generator.h"

using namespace generator;
using namespace maze_util;

void prims::generate() {
  /* Set every cell to have a wall on every side, and every cell as unvisited */
  pr_maze.set_all_walls();
  pr_maze.set_cells_unvisited();
  pr_maze.set_initialised();
  
  /* Choose a random cell to start prims on */
  range xr(0, pr_width-1), yr(0, pr_height-1);
  unsigned x = xr(pr_rng), y = yr(pr_rng);
  mark_cell(x, y);
  
  while (!pr_adjacent.empty()) {
    /* Choose and delete a cell at random from the adjacent list,
     * then choose a neigboring cell at random. */
    range ar(0, pr_adjacent.size()-1);
    unsigned ai = ar(pr_rng);
    cell &c = pr_adjacent[ai];
    
    std::vector<wall> nl = get_neigbors(c.x, c.y);
    range nr(0, nl.size()-1);
    unsigned ni = nr(pr_rng);
    wall &n = nl[ni];
    
    /* Delete the wall between this cell and the chosen neigbor, then
     * mark the neigbor and delete this cell from the adjacent list. */
    pr_maze.set_wall(c.x, c.y, n.d, false);
    mark_cell(c.x, c.y);
    pr_adjacent.erase(pr_adjacent.begin() + ai);
  }
}

void prims::mark_cell(const unsigned x, const unsigned y) {
  /* Mark this cell as visited. */
  pr_maze.get_cell(x, y).visited = true;
  
  /* Try and add neigboring cells to the list if they will be in bounds */
  if (x > 0) {
    add_adjacent(x-1, y);
  }
  
  if (x < pr_width-1) {
    add_adjacent(x+1, y);
  }
  
  if (y > 0) {
    add_adjacent(x, y-1);
  }
  
  if (y < pr_height-1) {
    add_adjacent(x, y+1);
  }
}

void prims::add_adjacent(const unsigned &x, const unsigned &y) {
  /* If the cell hasn't been already visited,
   * we will add it to our adjacent list. */
  cell &c = pr_maze.get_cell(x, y);
  
  if (!c.visited && !c.path) {
    c.path = true;
    pr_adjacent.push_back(c);
  }
}

std::vector<wall> prims::get_neigbors(const unsigned &x, const unsigned &y) {
  std::vector<wall> n;
  
  if (x > 0) {
    cell &c = pr_maze.get_cell(x-1, y);
    
    if (c.visited) {
      n.push_back(wall(c.x, c.y, direction::EAST));
    }
  }
  
  if (x < pr_width-1) {
    cell &c = pr_maze.get_cell(x+1, y);
    
    if (c.visited) {
      n.push_back(wall(x+1, y, direction::WEST));
    }
  }
  
  if (y > 0) {
    cell &c = pr_maze.get_cell(x, y-1);
    
    if (c.visited) {
      n.push_back(wall(x, y-1, direction::NORTH));
    }
  }
  
  if (y < pr_height-1) {
    cell &c = pr_maze.get_cell(x, y+1);
    
    if (c.visited) {
      n.push_back(wall(x, y+1, direction::SOUTH));
    }
  }
  
  return n;
}

