#include "error.h"
#include "stopwatch.h"
#include "maze.h"
#include "generator.h"

#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>

/* Input flags helpers.
 * If you want to know what these codes mean,
 * please check the application help file. */
enum flag : unsigned char {
  H = 0,
  LB, SB, SV,
  GRD, GRB, GP, GK,
  PWF, PA, PT,
  TS, TL, TE,
  NONE, ERR, END
};

const size_t NUM_FLAGS = (size_t)flag::NONE;

const char *flag_str[NUM_FLAGS] = {
  "h",
  "lb", "sb", "sv",
  "grd", "grb", "gp", "gk",
  "pwf", "pa", "pt",
  "ts", "tl", "te"
};

/* Checks if the argument we are looing at is a flag or not,
 * if it is, it will return the equivilent flag enum. */
flag check_flag(const char *arg) {
  flag ret = flag::NONE;

  if (arg[0] == '-' && arg[1] == '-') {
    ret = flag::ERR;

    for(size_t i = 0; i < NUM_FLAGS; ++i) {
      if (strcmp(&arg[2], flag_str[i]) == 0) {
        return (flag)i;
      }
    }
  }

  return ret;
}

int main(int argc, char **argv) {
  /* Check if any args were parsed, if not terminate... */
  if (argc <= 1) {
    error::terminate(error::type::ARGS);
  }

  flag prev_flag = flag::ERR;
  int prev_flag_pos = -1;
  stopwatch *sw = nullptr;
  maze m;

  /* Iterate over every argument */
  for (int i = 1; i <= argc; ++i) {
    /* Set a temp flag for when we are iterating over:
    * an input flag (and what type of flag it is),
    * a sub argument for a flag, or,
    * we are finished iterating. */
    flag itr = flag::END;

    if (i < argc) {
      itr = check_flag(argv[i]);
    }

    if (itr == flag::NONE) {
      /* If we are trying to parse a non flag argument without any flags first,
       * it's time to stop. */
      if (i == 1) {
        error::terminate(error::type::ARGS, argv[i]);
      }
    }
    else {
      /* Continue if the first arg is a flag, we'll process it upon hitting
       * the next flag after finding it's sub arguments. (if any) */
      if (i == 1) {
        prev_flag = itr;
        prev_flag_pos = 1;
        continue;
      }

      /* We are now finished processing the previous flags arguments,
       * you are here: --<previous flag> ...args... --<current flag>
       * we need to deal with the previous flag and it's arguments.
       * In other words, this is where the input arguments are actually
       * translated into calling methods. */
      int num_sub_args = i - prev_flag_pos - 1;

      switch(prev_flag) {
      case flag::H:
        error::terminate(error::type::HELP);
        break;
      case flag::LB:
        if (num_sub_args != 1) {
          error::terminate(error::type::NUM_ARGS, argv[prev_flag_pos]);
        }

        m.load_bin(argv[prev_flag_pos+1]);
        break;
      case flag::SB:
        if (num_sub_args != 1) {
          error::terminate(error::type::NUM_ARGS, argv[prev_flag_pos]);
        }

        m.save_bin(argv[prev_flag_pos+1]);
        break;
      case flag::SV:
        if (num_sub_args != 1) {
          error::terminate(error::type::NUM_ARGS, argv[prev_flag_pos]);
        }

        m.save_svg(argv[prev_flag_pos+1]);
        break;
      case flag::GRB:
        generator::re_back(m, num_sub_args+1, &argv[prev_flag_pos]).generate();
        break;
      case flag::GRD:
        generator::re_div(m, num_sub_args+1, &argv[prev_flag_pos]).generate();
        break;
      case flag::GP:
        generator::prims(m, num_sub_args+1, &argv[prev_flag_pos]).generate();
        break;
      case flag::GK:
        generator::kruskals(m, num_sub_args+1, &argv[prev_flag_pos]).generate();
        break;
      case flag::PWF:
        std::cout << "Wall following path finding not yet implimented...\n";
        break;
      case flag::PA:
        std::cout << "a* path finding not yet implimented...\n";
        break;
      case flag::PT:
        std::cout << "Tremaux's path finding not yet implimented...\n";
        break;
      case flag::TS:
        if (sw == nullptr) {
          sw = new stopwatch();
        }
        else {
          std::cout << "A stopwatch is already running. Ignoring input.\n";
        }
        break;
      case flag::TL:
        if (sw != nullptr) {
          sw->lap();
        }
        else {
          std::cout << "There is no stopwatch running. Ignoring input.\n";
        }
        break;
      case flag::TE:
        if (sw != nullptr) {
          delete sw;
          sw = nullptr;
        }
        else {
          std::cout << "There is no stopwatch running. Ignoring input.\n";
        }
        break;
      default:
        error::terminate(error::type::ARGS, argv[prev_flag_pos]);
        break;
      };
      
      /* Set the previous flag and iterator position to be the current flag. */
      prev_flag = itr;
      prev_flag_pos = i;
    }
  }

  /* If the user did not end the stopwatch, do that before exiting. */
  if (sw != nullptr) {
    delete sw;
    sw = nullptr;
  }

  return EXIT_SUCCESS;
}

