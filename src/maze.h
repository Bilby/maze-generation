#pragma once

#include <vector>
#include <string>

namespace maze_util {
  /* Alias for 2D std::vectors. */
  template <typename T>
  using vector2d = typename std::vector<std::vector<T>>;

  /* Enum for representing cardinal direction. */
  enum direction : unsigned char {
    NORTH = 0, SOUTH, EAST, WEST, INVALID
  };

  const unsigned char NUM_DIRS = direction::INVALID;

  inline direction operator !(const direction &d) {
    switch (d) {
    case direction::NORTH:
      return direction::SOUTH;
    case direction::SOUTH:
      return direction::NORTH;
    case direction::EAST:
      return direction::WEST;
    case direction::WEST:
      return direction::EAST;
    default:
      return direction::INVALID;
    }
  }

  /* Define the maze egde and cell. */
  struct edge {
    edge() : wall(false), visited(false) {}
    edge(const bool &_wall) : wall(_wall), visited(false) {}

    bool wall, visited;
  };

  struct cell {
    cell() : x(0), y(0), path(false), visited(false) {}
    cell(const unsigned &_x, const unsigned &_y) :
      x(_x), y(_y), path(false), visited(false) {}

    unsigned x, y;
    bool path, visited;
    edge adjacent[NUM_DIRS];
  };

  struct pointf {
    pointf() : pr_x(-1.0), pr_y(-1.0) {}
    pointf(const unsigned&, const unsigned&, const unsigned&, const unsigned&);
    std::string to_string(const unsigned&);
   
   private:
    float pr_x, pr_y;
  };

  struct linef {
    linef(const cell&, const direction&, const unsigned&, const unsigned&);
    linef(const cell&, const unsigned&, const unsigned&, const bool&);

    friend std::ostream &operator <<(std::ostream&, const linef&);

    pointf first, second;
    const char *color;
  };
}

/* Decleration of the maze structure. */
struct maze {
  /* Default constructor reserves no memory use for cells initially. */
  maze() : pr_width(0), pr_height(0), pr_initialised(false), pr_cells(0) {}

  /* Constructor to reserve memory for all cells based on width and height. */
  maze(const unsigned &width, const unsigned &height);

  /* Getters */
  bool is_initialised() { return pr_initialised; }
  maze_util::vector2d<maze_util::cell> &get_cells() { return pr_cells; }
  const unsigned &get_height() { return pr_height; }
  const unsigned &get_width() { return pr_width; }
  unsigned get_edge_count();
  maze_util::cell &get_cell(const unsigned &x, const unsigned &y) {
    return pr_cells[y][x];
  }
  
  maze_util::cell *get_neigbor(
    const unsigned&, const unsigned&, const maze_util::direction&
  );

  /* Setter methods. */
  void set_walls_unvisited();
  void set_cells_unvisited();
  void set_all_walls(const bool &is_wall=true);
  void set_initialised() { pr_initialised = true; }

  /* Modification methods. */
  void set_wall(
    const unsigned&, const unsigned&, const maze_util::direction&,
    const bool &on=true
  );

  void visit_edge(maze_util::cell&, const maze_util::direction&);
  void resize(const unsigned&, const unsigned&);

  /* File IO. (defs in maze_io.cpp) */
  void load_bin(const char*);
  void save_bin(const char*);
  void save_svg(const char*);

private:
  unsigned pr_width, pr_height;
  bool pr_initialised;
  maze_util::vector2d<maze_util::cell> pr_cells;
};

