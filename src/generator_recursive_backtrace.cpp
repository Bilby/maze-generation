#include "generator.h"

using namespace generator;
using namespace maze_util;

void re_back::generate() {
  pr_maze.set_all_walls();
  pr_maze.set_cells_unvisited();
  pr_maze.set_initialised();
  
  /* Choose a random cell to start recursive backtrace on. */
  range xr(0, pr_width-1), yr(0, pr_height-1);
  unsigned x = xr(pr_rng), y = yr(pr_rng);
  carve_from(pr_maze.get_cell(x, y));
}

void re_back::carve_from(cell &c) {
  std::vector<direction> dirs(NUM_DIRS);
  dirs[0] = direction::NORTH;
  dirs[1] = direction::SOUTH;
  dirs[2] = direction::EAST;
  dirs[3] = direction::WEST;
  
  while (!dirs.empty()) {
    /* Choose a random direction. */
    range dr(0, dirs.size()-1);
    unsigned di = dr(pr_rng);
    direction &d = dirs[di];
    
    /* Get the neigbor to this cell given the direction. */
    cell *n = pr_maze.get_neigbor(c.x, c.y, d);
    
    /* If the neigbor is valid, and unvisited we can recurively continue
     * carving a path from it, and delete the wall between this cell and it. */
    if (n != nullptr && !n->visited && !n->path) {
      c.path = true;
      n->visited = true;
      pr_maze.set_wall(c.x, c.y, d, false);
      carve_from(*n);
    }
    
    /* Remove this direction form the directions list. */
    dirs.erase(dirs.begin() + di);
  }
}

