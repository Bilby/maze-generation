#include "stopwatch.h"

#include <iostream>

stopwatch::stopwatch() {
  std::cout << "-----------------------------------------------------\n"
    "stopwatch starting.\n"
    "-----------------------------------------------------\n";

  pr_laps.push_back(std::chrono::steady_clock::now());
}

stopwatch::~stopwatch() {
  std::cout << "-----------------------------------------------------\n"
    "Stopwatch stopping.\n";
  
  pr_laps.push_back(std::chrono::steady_clock::now());

  /* If there were laps, print those out before the final time... */
  if (pr_laps.size() > 2) {
    for (size_t i = 1; i < pr_laps.size(); ++i) {
      std::cout << " * Stopwatch lap [" << i << "]: " <<
        std::chrono::duration_cast<std::chrono::milliseconds>(
          pr_laps[i] - pr_laps[i - 1]
        ).count() << "ms.\n";
    }
  }

  std::cout << "The stopwatch ran for " <<
    std::chrono::duration_cast<std::chrono::milliseconds>(
      pr_laps[pr_laps.size() - 1] - pr_laps[0]
    ).count() << "ms.\n"
    "-----------------------------------------------------\n";
}

void stopwatch::lap() {
  pr_laps.push_back(std::chrono::steady_clock::now());
  std::cout << "Starting stopwatch lap " << pr_laps.size() << ".\n";
}

