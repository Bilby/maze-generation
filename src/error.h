#pragma once

namespace error {
  enum class type {
    ARGS, NOT_FLAG, NUM_ARGS, HELP,
    LOAD, SAVE, NOT_INIT, OOB, INVALID
  };

  void terminate(const type&, const char *msg=nullptr);
}

