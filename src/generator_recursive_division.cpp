#include "generator.h"

using namespace generator;
using namespace maze_util;

void re_div::generate() {
  unsigned w = pr_width - 1, h = pr_height - 1;
  pr_maze.set_all_walls(false);
  pr_maze.set_initialised();

  /* Fill out the perimiter walls. */
  for (unsigned x = 0; x < pr_width; ++x) {
    pr_maze.set_wall(x, 0, direction::NORTH);
    pr_maze.set_wall(x, h, direction::SOUTH);
  }
  
  for (unsigned y = 0; y < pr_height; ++y) {
    pr_maze.set_wall(0, y, direction::EAST);
    pr_maze.set_wall(w, y, direction::WEST);
  }
  
  /* Insert the whole maze as the initial field on our stack. */
  pr_fields.push(field(0, 0, w, h));
  
  /* While there are valid fields to divide... */
  while (!pr_fields.empty()) {
    /* Grab and pop off the last stack field element. */
    field f = pr_fields.top();
    pr_fields.pop();
    
    /* Work out the width and height of the field */
    unsigned w = f.x2 - f.x1, h = f.y2 - f.y1;
    
    /* Don't work on this field if it is at a minimum size. */
    if (w < GRANULARITY || h < GRANULARITY) {
      continue;
    }
    
    /* If a field is higher than wide, bisect horizontally,
     * otherwise bisect vertically. */
    bool hor = (w < h);
    
    if (hor) {
      /* Create a wall along the randomly selected y pos,
       * with a gap at the randomly selected x pos. */
      range xr(f.x1, f.x2), yr(f.y1, f.y2-1);
      unsigned xi = xr(pr_rng), yi = yr(pr_rng);
      
      for (unsigned xx = f.x1; xx <= f.x2; ++xx) {
        if (xx != xi) {
          pr_maze.set_wall(xx, yi, direction::SOUTH);
        }
      }
      
      /* Insert the two subfields to the stack */
      pr_fields.push(field(f.x1, f.y1, f.x2, yi));
      pr_fields.push(field(f.x1, yi+1, f.x2, f.y2));
    }
    else {
      /* Create a wall along the randomly selected x pos,
       * with a gap at the randomly selected y pos. */
      range xr(f.x1, f.x2-1), yr(f.y1, f.y2);
      unsigned xi = xr(pr_rng), yi = yr(pr_rng);
      
      for (unsigned yy = f.y1; yy <= f.y2; ++yy) {
        if (yy != yi) {
          pr_maze.set_wall(xi, yy, direction::WEST);
        }
      }
      
      /* Insert the two subfields to the stack */
      pr_fields.push(field(f.x1, f.y1, xi, f.y2));
      pr_fields.push(field(xi+1, f.y1, f.x2, f.y2));
    }
  }
}

