#include "maze.h"
#include "error.h"

#include <sstream>

using namespace maze_util;

maze::maze(const unsigned &w, const unsigned &h) :
    pr_width(w), pr_height(h), pr_initialised(false), pr_cells(h) {
  for (auto &y : pr_cells) {
    y.resize(w);
  }
}

unsigned maze::get_edge_count() {
  unsigned ret = 0;

  for (auto &y : pr_cells) {
    for (auto &x : y) {
      for (int i = 0; i < NUM_DIRS; ++i) {
        if (!x.adjacent[i].visited) {
          ++ret;
          visit_edge(x, (direction)i);
        }
      }
    }
  }

  return ret;
}

cell *maze::get_neigbor(const unsigned &x, const unsigned &y,
    const direction &d) {
  switch(d) {
  case direction::NORTH:
    if (y > 0) {
      return &pr_cells[y-1][x];
    }

    break;
  case direction::SOUTH:
    if (y < pr_height-1) {
      return &pr_cells[y+1][x];
    }

    break;
  case direction::EAST:
    if (x > 0) {
      return &pr_cells[y][x-1];
    }

    break;
  case direction::WEST:
    if (x < pr_width-1) {
      return &pr_cells[y][x+1];
    }

    break;
  default:
    return nullptr;
  }

  return nullptr;
}

void maze::set_walls_unvisited() {
  for (auto &y : pr_cells) {
    for (auto &x : y) {
      for (auto &e : x.adjacent) {
        e.visited = false;
      }
    }
  }
}

void maze::set_cells_unvisited() {
  for (auto &y : pr_cells) {
    for (auto &x : y) {
      x.visited = false;
      x.path = false;
    }
  }
}

void maze::set_all_walls(const bool &is_wall) {
  for (auto &y : pr_cells) {
    for (auto &x : y) {
      for (auto &e : x.adjacent) {
        e.wall = is_wall;
      }
    }
  }
}

void maze::set_wall(
    const unsigned &x, const unsigned &y, const direction &d,
    const bool &on){
  /* Set the current cells edge if it's in bounds. */
  if (x < pr_width && y < pr_height) {
    pr_cells[y][x].adjacent[d].wall = on;
  }

  /* Set the oppiset edge on the neighboring cell if it's in bounds. */
  cell *n = get_neigbor(x, y, d);
  
  if (n != nullptr) {
    n->adjacent[!d].wall = on;
  }
}

void maze::visit_edge(cell &c, const direction &d) {
  /* Set the direction side of this cell to visited. */
  c.adjacent[d].visited = true;

  /* Set the opposite direction of the neigboring cell to visited
   * provided there is a neigboring cell (we aren't already on the edge). */
  cell *n = get_neigbor(c.x, c.y, d);

  if (n != nullptr) {
    n->adjacent[!d].visited = true;
  }
}

void maze::resize(const unsigned &w, const unsigned &h) {
  if (pr_height != h) {
    pr_cells.resize(h);
  }

  for (unsigned y = 0; y < h; ++y) {
    if (pr_width != w) {
      pr_cells[y].resize(w);
    }

    /* Update all of the coordinates of new cells if the maze is expanded. */
    for (unsigned x = (y < pr_height) ? pr_width : 0; x < w; ++x) {
      pr_cells[y][x].x = x;
      pr_cells[y][x].y = y;
    }
  }

  pr_width = w;
  pr_height = h;
}

