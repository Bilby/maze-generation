#include "generator.h"
#include "error.h"

using namespace generator;
using namespace maze_util;

kruskals::~kruskals() {
  for (auto &d : pr_cells) {
    delete d;
  }
}

void kruskals::generate() {
  /* Set every cell to have a wall on every side. */
  pr_maze.set_all_walls();
  pr_maze.set_initialised();

  /* Add all walls (except for the exterior) to the list of walls.
   * Also add every cell to a list with a set containing just itself. */
  unsigned h = pr_height - 1, w = pr_width - 1, max = pr_height * pr_width;
  pr_cells.resize(max);

  for (unsigned y = 0; y < pr_height; ++y) {
    for (unsigned x = 0; x < pr_width; ++x) {
      /* Only push the south and west walls, since we only need those.
       * Don't insert the last ones either, as they are for the border. */
      if (y < h) {
        pr_walls.push_back(wall(x, y, direction::SOUTH));
      }

      if (x < w) {
        pr_walls.push_back(wall(x, y, direction::WEST));
      }

      /* The key to insert is the representation of x/y in a flat array. */
      unsigned id = get_id(x, y);
      pr_cells[id] = new avl_tree<unsigned>();
      pr_cells[id]->insert(id);
    }
  }

  while (!pr_walls.empty()) {
    /* Randomly select a wall element. */
    range wr(0, pr_walls.size()-1);
    unsigned wi = wr(pr_rng);
    wall &w = pr_walls[wi];

    /* Get neigboring cell on the other side of the wall. */
    cell *n = pr_maze.get_neigbor(w.x, w.y, w.d);

    /* Terminate is we somehow managed to go out of bounds with the neigbor. */
    if (n == nullptr) {
      error::terminate(error::type::OOB);
    }

    /* Check if both cells on each side of the wall are in unique sets. */
    unsigned c_id = get_id(w.x, w.y), n_id = get_id(n->x, n->y),
      c = pr_cells.size();
      
    if (c < 2) {
      /* Every cell is in the same set. */
      break;
    }
    
    /* -1 on an unsigned is the maximum possible value, so we are using that
     * for error checking here. */
    size_t ci = -1, ni = -1, di = -1, err = -1;

    /* Search through every set to find which one the current and neigboring
     * cell belong to. */
    for (size_t i = 0; i < c; ++i) {
      if (pr_cells[i]->find(c_id) != nullptr) {
        ci = i;
      }

      if (pr_cells[i]->find(n_id) != nullptr) {
        ni = i;
      }

      if (ci != err && ni != err) {
        break;
      }
    }
    
    /* Assert both the current and neigbor cell were found in any set. */
    if (ci == err || ni == err) {
      error::terminate(error::type::OOB);
    }

    if (ci != ni) {
      /* Delete the current wall in the maze. */
      pr_maze.set_wall(w.x, w.y, w.d, false);

      /* Join the neigbor set with the current cell set. */
      avl_tree<unsigned> *small_tree = nullptr, *big_tree = nullptr;

      /* Get the smaller and larger of the two trees. */
      if (pr_cells[ci]->get_size() >= pr_cells[ni]->get_size()) {
        small_tree = pr_cells[ci];
        big_tree = pr_cells[ni];
        di = ci;
      }
      else {
        big_tree = pr_cells[ci];
        small_tree = pr_cells[ni];
        di = ni;
      }

      /* Get all of the elements of the small tree as a flat ordered array */
      small_tree->update_flat_array_ordered();
      auto *flat = small_tree->get_flat_array();
      
      /* Insert every element of the small tree into the big tree. */
      for (auto node : *flat) {
        big_tree->insert(node->key);
      }

      /* Remove the small set from the set list. */
      delete pr_cells[di];
      pr_cells.erase(pr_cells.begin() + di);
    }

    /* Remove the wall from our wall list. */
    pr_walls.erase(pr_walls.begin() + wi);
  }
}

