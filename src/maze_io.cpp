#include "maze.h"
#include "error.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace maze_util;
using namespace std;

void maze::load_bin(const char *f_name) {
  cout << "Loading the maze file \"" << f_name << "\".\n";
  ifstream f_stream;
  f_stream.exceptions(ios::badbit | ios::failbit);
  
  try {
    unsigned w, h;
    char in;

    /* Read in the width and height. */
    f_stream.open(f_name, ios::in | ios::binary);
    f_stream.read((char*)&w, sizeof(unsigned));
    f_stream.read((char*)&h, sizeof(unsigned));
    resize(w, h);

    /* Read in the rest of the cell walls. */
    for (unsigned y = 0; y < h; ++y) {
      for (unsigned x = 0; x < w; ++x) {
        for (unsigned i = 0; i < NUM_DIRS; ++i) {
          f_stream.read(&in, sizeof(char));
          
          if (in) {
            set_wall(x, y, direction(i));
          }
        }
      }
    }

    pr_initialised = true;
  }
  catch (const std::exception& ex) {
    error::terminate(error::type::LOAD, f_name);
  }

  f_stream.close();
}

void maze::save_bin(const char *f_name) {
  cout << "Saving maze file to \"" << f_name << "\".\n";

  if (!pr_initialised) {
    error::terminate(error::type::NOT_INIT);
  }

  ofstream f_stream;
  f_stream.exceptions(ios::badbit | ios::failbit);

  try {
    /* Write the width and height to the file. */
    f_stream.open(f_name, ios::out | ios::binary);
    f_stream.write((char*)&pr_width, sizeof(unsigned));
    f_stream.write((char*)&pr_height, sizeof(unsigned));
    set_walls_unvisited();

    for (auto &y : pr_cells) {
      for (auto &x : y) {
        for (unsigned i = 0; i < NUM_DIRS; ++i) {
          auto &e = x.adjacent[i];
          char out = 0;

          if (!e.visited) {
            if (e.wall) {
              out = 1;
            }

            visit_edge(x, direction(i));
          }

          f_stream.write(&out, sizeof(char));
        }
      }
    }
  }
  catch (const std::exception& ex) {
    error::terminate(error::type::SAVE, f_name);
  }

  f_stream.close();
}

pointf::pointf(const unsigned &x, const unsigned &y,
    const unsigned &width, const unsigned &height) {
  pr_x = (float)x / (float)width;
  pr_y = (float)y / (float)height;
}

string pointf::to_string(const unsigned &n) {
  stringstream ss;
  ss << " x" << n << "='" << pr_x << "' y" << n << "='" << pr_y << "'";
  return ss.str();
}

linef::linef(const cell &c, const direction &d,
    const unsigned &width, const unsigned &height) : color("white") {
  switch(d) {
  case direction::NORTH:
    first = pointf(c.x, c.y, width, height);
    second = pointf(c.x+1, c.y, width, height);
    break;
  case direction::SOUTH:
    first = pointf(c.x, c.y+1, width, height);
    second = pointf(c.x+1, c.y+1, width, height);
    break;
  case direction::EAST:
    first = pointf(c.x, c.y, width, height);
    second = pointf(c.x, c.y+1, width, height);
    break;
  case direction::WEST:
    first = pointf(c.x+1, c.y, width, height);
    second = pointf(c.x+1, c.y+1, width, height);
    break;
  default:
    cerr << "[ERROR] Unknown direction given to the point creator."
      "Ignoring.\n";
    break;
  }
}

linef::linef(const cell &c, const unsigned &width,
    const unsigned &height, const bool &u) : color("red") {
  if (u) {
    first = pointf(c.x, c.y, width, height);
    second = pointf(c.x+1, c.y+1, width, height);
  }
  else {
    first = pointf(c.x+1, c.y, width, height);
    second = pointf(c.x, c.y+1, width, height);
  }
}

ostream &operator << (ostream &os, linef &l) {
  // TODO: Make the stroke width and other SVG values non magic numbers...
  os << "<line stroke='" << l.color << "' stroke-width='0.005'" <<
    l.first.to_string(1) << l.second.to_string(2) << "/>";
  return os;
}

void maze::save_svg(const char *f_name) {
  cout << "Saving maze as an SVG file to \"" << f_name << "\".\n";

  if (!pr_initialised) {
    error::terminate(error::type::NOT_INIT);
  }

  ofstream f_stream;
  f_stream.exceptions(ios::badbit | ios::failbit);
  set_walls_unvisited();

  if (!f_stream.good()) {
    error::terminate(error::type::SAVE, f_name);
  }

  /* SVG header */
  try {
    f_stream.open(f_name, ios::out);
    f_stream << "<svg viewBox='0 0 1 1' width='500' height='500' "
      "xmlns='http://www.w3.org/2000/svg'>\n"
      "<rect width='1' height='1' style='fill: black' />\n";

    /* Print maze walls and cells to the SVG file. */
    for (auto &y : pr_cells) {
      for (auto &x : y) {
        /* Print the solution path cells in red. */
        if (x.path) {
          linef u(x, pr_width, pr_height, true);
          linef d(x, pr_width, pr_height, false);
        }

        /* Print any walls we haven't already. */
        for (unsigned i = 0; i < NUM_DIRS; ++i) {
          auto &e = x.adjacent[i];

          if (!e.visited) {
            if (e.wall) {
              linef l(x, direction(i), pr_width, pr_height);
              f_stream << l << "\n";
            }

            visit_edge(x, direction(i));
          }
        }
      }
    }
  }
  catch (const std::exception& ex) {
    error::terminate(error::type::SAVE, f_name);
  }

  /* SVG footer */
  f_stream << "</svg>";
}

