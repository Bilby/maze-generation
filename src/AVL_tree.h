#pragma once

#define M_MAX(a, b) ((a) > (b)) ? (a) : (b)

#include <vector>

template <typename K>
struct leaf {
  leaf(K _key) : key(_key), left(nullptr), right(nullptr), height(1) {}

  K key;
  leaf *left, *right;
  u_int32_t height;
};

template <typename K>
struct avl_tree {
  avl_tree() : pr_size(0), pr_root(nullptr) {}
  ~avl_tree() { empty(); }
  
  leaf<K> *get_root() { return pr_root; }

  size_t get_size() { return pr_size; }

  /* update_flat_array_ordered() or update_flat_array_pre_ordered()
   * must be called after every insertion or removal for this pointer
   * to be accurate. */
  std::vector<leaf<K>*> *get_flat_array() { return &pr_flat_array; }

  /* update_flat_array_pre_ordered, really only useful for debugging and
   * testing, and will overide pr_flat_array. */
  void update_flat_array_pre_ordered() {
    pr_flat_array.clear();
    fill_pre_ordered_array(pr_root);
  }

  void update_flat_array_ordered() {
    /* Clear and refill our ordered array. */
    pr_flat_array.clear();
    fill_ordered_array(pr_root);
  }

  void insert(K key) {
    pr_root = insert_recursive(pr_root, key);
  }
  
  /* remove(K key) removes the first instance of 'key' found in the tree.
   * Returns false if the node to delete could not be found, or this tree
   * currently has no elements. */
  bool remove(K key) {
    return ((pr_root = remove_recursive(pr_root, key)) != nullptr);
  }

  leaf<K> *find(K key) { return find_recursive(key, pr_root); }

  /* Leaf find overloader, returns nullptr if key not found. */
  leaf<K> *operator[](K key) {
    return find(key);
  }
  
  void empty() {
    empty_recursive(pr_root);
    pr_root = nullptr;
  }

private:
  void fill_ordered_array(leaf<K> *node) {
    /* Stop if the node we are on is null. */
    if (node != nullptr) {
      /* Run function recursive on the left of the node. */
      fill_ordered_array(node->left);

      /* Add our current node to the back of the array. */
      pr_flat_array.push_back(node);

      /* Run function recursive on the right of the node. */
      fill_ordered_array(node->right);
    }
  }

  void fill_pre_ordered_array(leaf<K> *node) {
    /* Stop if the node we reach is null. */
    if (node != nullptr) {
      /* Add our current node to the back of the array. */
      pr_flat_array.push_back(node);

      /* Run our function recursive of the left and right of the node. */
      fill_pre_ordered_array(node->left);
      fill_pre_ordered_array(node->right);
    }
  }

  leaf<K> *insert_recursive(leaf<K> *node, K key) {
    /* If the current node is null, we create our node here. */
    if (node == nullptr) {
      ++pr_size;
      return new leaf<K>(key);
    }

    /* Step down to either the left or right subtree depending on the key,
     * to find the correct place for the new node. */
    if (key < node->key) {
      node->left = insert_recursive(node->left, key);
    }
    else if (key > node->key) {
      node->right = insert_recursive(node->right, key);
    }
    else {
      return node;
    }

    /* Update the height of this current ancestor node. */
    node->height = M_MAX(get_node_height(node->left),
      get_node_height(node->right)) + 1;

    /* Check if the node has become unbalanced. */
    int32_t bal = get_node_balance(node);

    /* Left left case. */
    if (bal > 1 && key < node->left->key) {
      return right_rotate(node);
    }

    /* Right right case. */
    if (bal < -1 && key > node->right->key) {
      return left_rotate(node);
    }

    /* Left right case. */
    if (bal > 1 && key > node->left->key) {
      node->left = left_rotate(node->left);
      return right_rotate(node);
    }

    /* Right left case. */
    if (bal < -1 && key < node->right->key) {
      node->right = right_rotate(node->right);
      return left_rotate(node);
    }

    /* Return unchanged node. */
    return node;
  }

  leaf<K> *remove_recursive(leaf<K> *node, K key) {
    /* If the current node is null, we just return it straight up. */
    if (node == nullptr) {
      return node;
    }

    /* Step down to either the left or right subtree to find the node to
     * remove. */
    if (key < node->key) {
      node->left = remove_recursive(node->left, key);
    }
    else if (key > node->key) {
      node->right = remove_recursive(node->right, key);
    }
    else {
      /* The key for deletion is the same as the node's key. This is the
       * confirmed node we are going to delete.
       * We can now check to see if there are children nodes from the soon to
       * be deleted node. */
      if ((node->left == nullptr) || (node->right == nullptr)) {
        /* Either one or no children.
         * Grab the one child or nullptr as a temporary variable. */
        leaf<K> *child = (node->left == nullptr) ? node->right : node->left;

        if (child == nullptr) {
          /* Handle no children case. */
          delete node;
          node = nullptr;

          --pr_size;
        }
        else {
          /* Handle the one child case, delete the node */
          delete node;
          node = child;

          --pr_size;
        }
      }
      else {
        /* We have two children, get the smallest node in the right subtree. */
        leaf<K> *tmp = get_smallest_node(node->right);

        /* Copy that nodes key over to be our current nodes key. */
        node->key = tmp->key;
        node->height = tmp->height;

        /* Delete that node. */
        node->right = remove_recursive(node->right, tmp->key);
      }
    }

    /* Return early if the tree only had one node. */
    if (node == nullptr) {
      return node;
    }

    /* Update the height of the current node. */
    node->height = M_MAX(get_node_height(node->left),
      get_node_height(node->right)) + 1;

    /* Check if the node has become unbalanced. */
    int32_t bal = get_node_balance(node);

    if (bal > 1 && get_node_balance(node->left) >= 0) {
      return right_rotate(node);
    }

    if (bal > 1 && get_node_balance(node->left) < 0) {
      node->left = left_rotate(node->left);
      return right_rotate(node);
    }

    if (bal < -1 && get_node_balance(node->right) <= 0) {
      return left_rotate(node);
    }

    if (bal > 1 && get_node_balance(node->right) > 0) {
      node->right = right_rotate(node->right);
      return left_rotate(node);
    }

    return node;
  }

  leaf<K> *find_recursive(K key, leaf<K> *node) {
    if (node == nullptr) {
      return node;
    }

    if (key > node->key) {
      return find_recursive(key, node->right);
    }
    else if (key < node->key) {
      return find_recursive(key, node->left);
    }
    
    return node;
  }

  void empty_recursive(leaf<K> *node) {
    if (node != nullptr) {
      /* Recursivley empty the subtrees of the left and right children nodes. */
      empty_recursive(node->left);
      empty_recursive(node->right);

      /* Delete the current node. */
      delete node;
      node = nullptr;
    }
  }

  leaf<K> *get_smallest_node(leaf<K> *node) {
    leaf<K> *tmp = node;

    /* Loop to find the leftmost leaf. */
    while (tmp->left != nullptr) {
      tmp = tmp->left;
    }

    return tmp;
  }

  /* Function to right rotate a subtree with root y. */
  leaf<K> *right_rotate(leaf<K> *y) {
    leaf<K> *x = y->left;
    leaf<K> *z = x->right;

    /* Do rotation. */
    x->right = y;
    y->left = z;

    /* Update new heights. */
    y->height = M_MAX(get_node_height(y->left), get_node_height(y->right)) + 1;
    x->height = M_MAX(get_node_height(x->left), get_node_height(x->right)) + 1;

    return x;
  }

  /* Function to left rotate a subtree with root x. */
  leaf<K> *left_rotate(leaf<K> *x) {
    leaf<K> *y = x->right;
    leaf<K> *z = y->left;

    /* Do rotation. */
    y->left = x;
    x->right = z;

    /* Update new heights. */
    x->height = M_MAX(get_node_height(x->left), get_node_height(x->right)) + 1;
    y->height = M_MAX(get_node_height(y->left), get_node_height(y->right)) + 1;

    return y;
  }

  int32_t get_node_balance(leaf<K> *node) {
    if (node == nullptr) {
      return 0;
    }

    return get_node_height(node->left) - get_node_height(node->right);
  }

  u_int32_t get_node_height(leaf<K> *node) {
    return (node == nullptr) ? 0 : node->height;
  }

  size_t pr_size;

  leaf<K> *pr_root;

  std::vector<leaf<K>*> pr_flat_array;
};

