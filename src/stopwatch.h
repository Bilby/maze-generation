#pragma once

#include <chrono>
#include <vector>

struct stopwatch {
  stopwatch();
  ~stopwatch();

  void lap();
  
private:
  std::vector<std::chrono::steady_clock::time_point> pr_laps;
};

