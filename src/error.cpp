#include "error.h"

#include <iostream>
#include <cstdlib>

const char *help_msg = "The runtime arguments are:\n"
  "--lb [filename]: loads a binary file.\n"
  "--sb [filename]: saves a binary file.\n"
  "--sv [filename]: saves an SVG file.\n\n"
  "--grb [random seed (optional)] [width] [height]: "
  "generates a maze using recursive backtracing.\n"
  "--grd [random seed (optional)] [width] [height]: "
  "generates a maze using recursive division.\n"
  "--gp [random seed (optional)] [width] [height]: "
  "generates a maze using Prim's algorithm.\n"
  "--gk [random seed (optional)] [width] [height]: "
  "generates a maze using Kruskal's algorithm.\n\n"
  "--pwf: attempts to solve a maze using the \"Wall Follower\" algorithm.\n"
  "--pa: attemps to solve a maze using the A* algorithm.\n"
  "--pt: attemps to solve a maze using Tremaux's maze solver algorithm.\n\n"
  "--ts: starts a stopwatch.\n"
  "--tl: sets a lap on the stopwatch.\n"
  "--te: stops the stopwatch.\n";

const char *seek_help_msg = "See the help file, "
  "(run with --h) for more info.\n";

const char *error_msg = "<ERROR>";

void error::terminate(const type &error, const char *msg) {
  using namespace error;

  /* Switch over the error type, print an appropriate error msg, then
  * terminate. */
  switch(error) {
  case type::ARGS:
    if (msg == nullptr) {
      std::cerr << "[ERROR] No arguments were given. " << help_msg;
    }
    else {
      std::cerr << "[ERROR] Unexpected argument \"" <<
        ((msg != nullptr) ? msg : "")  << "\" given.\n" << help_msg;
    }
    break;
  case type::NOT_FLAG:
    std::cerr << "[ERROR] A flag argument was expected";

    if (msg != nullptr) {
      std::cerr << ", instead the input \"" << msg << "\" was given";
    }

    std::cerr << ".\n" << seek_help_msg;
    break;
  case type::NUM_ARGS:
    std::cerr << "[ERROR] An incorrect number of arguments were given for the "
      "flag \"" << ((msg != nullptr) ? msg : error_msg) << "\".\n";
    break;
  case type::HELP:
    std:: cerr << help_msg;
    break;
  case type::LOAD:
    std::cerr << "[ERROR] Could not load \"" <<
      ((msg != nullptr) ? msg : error_msg) << "\".\n";
    break;
  case type::SAVE:
    std::cerr << "[ERROR] Could not save \"" <<
      ((msg != nullptr) ? msg : error_msg) << "\".\n";
    break;
  case type::NOT_INIT:
    std::cerr << "[ERROR] Could not save the maze, as it is not "
      "yet initialised...\n";
    break;
  case type::OOB:
    std::cerr << "[ERROR] Out of bounds! " << ((msg != nullptr) ? msg : "");
    break;
  default:
    std::cerr << "[ERROR] An unexpected error occured.\n";
    break;
  }

  /* EXIT_SUCCESS since we want to tell the OS everything is fine.
   * We took care of it here. */
  exit(EXIT_SUCCESS);
}

