#include "generator.h"
#include "error.h"

#include <sstream>
#include <string>
#include <iostream>
#include <chrono>
#include <cstdlib>

using namespace generator;

base::base(maze &m, const int &argc, char **argv) : pr_maze(m) {
  /* Process user input. */  
  char *seed = nullptr;
  unsigned width = DEFAULT_WIDTH,
    height = DEFAULT_HEIGHT;
  
  if (argc <= 0) {
    error::terminate(error::type::NUM_ARGS);
    return;
  }
  
  // TODO: Further error processing if user inputs bad data...  
  switch (argc) {
  case 2:
    seed = argv[1];
    break;
  case 3:
    width = std::atol(argv[1]);
    height = std::atol(argv[2]);
    break;
  case 4:
    width = std::atol(argv[1]);
    height = std::atol(argv[2]);
    seed = argv[3];
    break;
  default:
    error::terminate(error::type::NUM_ARGS, argv[0]);
    return;
  }

  /* Check input and resize the maze accordingly */
  if (width < MIN_WIDTH || width > MAX_WIDTH) {
    std::stringstream ss;
    ss << "The width for maze generation must be between " << MIN_WIDTH <<
      " and " << MAX_WIDTH << ". Input of " << width << "given.";
    error::terminate(error::type::OOB, ss.str().c_str());
  }
  
  if (height < MIN_HEIGHT || height > MAX_HEIGHT) {
    std::stringstream ss;
    ss << "The height for maze generation must be between " << MIN_HEIGHT <<
      " and " << MAX_HEIGHT << ". Input of " << height << "given.";
    error::terminate(error::type::OOB, ss.str().c_str());
  }
  
  pr_maze.resize(width, height);
  
  /* Set the random number generator seed, if none was given,
   * we'll use system time. */
  if (seed == nullptr) {
    pr_seed = std::chrono::system_clock::now().time_since_epoch().count();
  }
  else {
    pr_seed = (std::atol(seed));
  }
  
  pr_width = width;
  pr_height = height;
  pr_rng.seed(pr_seed);
  
  std::cout << "Gernerating a new maze with size \"" <<
    width << "," << height << "\" and seed \"" << pr_seed << "\".\n";
}

