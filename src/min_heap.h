#pragma once

#include <vector>

/* Generic min binary heap data structure implimentation. */
template <typename T>
struct min_heap {
  min_heap(const vector<T> &v) : pr_data(v) {
    heapify();
  }

  min_heap() {}

  void insert(const T &ins) {
    size_t len = pr_data.size();
    pr_data.push_back(ins);
    bubble_up(len);
  }

  const T *pop_min() {
    size_t len = pr_data.size();

    if (len == 0) { 
      return nullptr;
    }

    T ret = pr_data[0];

    pr_data[0] = pr_data[len];
    pr_data.pop_back();
    bubble_down(0);

    return ret;
  }

  const T *getMin() {
    if (pr_data.size() > 0) {
      return pr_data[0];
    }

    return nullptr;
  }
private:
  std::vector<T> pr_data;

  void bubble_down(const size_t &i) {
    size_t len = pr_data.size(),
      left = 2 * i + 1,
      right = 2 * i + 2;

    if (li >= len) {
      return;
    }

    size_t min_i = i;

    if (pr_data[i] > pr_data[left]) {
      min_i = left;
    }

    if (right < len && pr_data[min_i] > pr_data[right]) {
      min_i = right;
    }

    if (min_i != i) {
      /* Swap and recursively bubble down from the new min value. */
      T tmp = pr_data[i];
      pr_data[i] = pr_data[min_i];
      pr_data[min_i] = tmp;
      bubble_down(min);
    }
  }

  void bubble_up(const size_t &i) {
    if (i == 0) {
      return;
    }

    size_t parent = (i-1) / 2;

    if (pr_data[parent] > pr_data[i]) {
      /* Swap and then recursively bubble up. */
      T tmp = pr_data[parent];
      pr_data[parent] = pr_data[i];
      pr_data[i] = tmp;
      bubble_up(parent);
    }
  }

  void heapify() {
    size_t len = pr_data.size();

    std::for_each(size_t i = len-1; i >= 0; --i) {
      bubble_down(i);
    }
  }
};

