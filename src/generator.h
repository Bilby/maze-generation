#pragma once

#include "maze.h"
#include "AVL_tree.h"

#include <random>
#include <stack>

namespace generator {
  /* Alias for random range. */
  using range = std::uniform_int_distribution<int>;

  /* Constants for maze generation. */
  const unsigned DEFAULT_WIDTH = 10,
    DEFAULT_HEIGHT = 10,
    MIN_WIDTH = 2,
    MIN_HEIGHT = 2,
    MAX_WIDTH = 3000,
    MAX_HEIGHT = 3000,
    GRANULARITY = 1;
  
  /* Wall element. */
  struct wall {
    wall() : x(0), y(0), d(maze_util::direction::INVALID) {}
    wall(const unsigned &_x, const unsigned &_y,
      const maze_util::direction &_d) : x(_x), y(_y), d(_d) {}
 
    unsigned x, y;
    maze_util::direction d;
  };
  
  /* Recursive division field */
  struct field {
    field(unsigned _x1, unsigned _y1, unsigned _x2, unsigned _y2) : x1(_x1),
      y1(_y1), x2(_x2), y2(_y2) {}
    
    unsigned x1, y1, x2, y2;
  };
  
  /* Base generator */
  struct base {
    base(maze&, const int&, char**);
    virtual void generate() {}

  protected:
    maze &pr_maze;
    unsigned pr_seed, pr_width, pr_height;
    std::mt19937 pr_rng;
    
    unsigned get_id(unsigned &x, unsigned &y) { return x + y * pr_width; }
  };
  
  /* Recursive backtrack generator */
  struct re_back : public base {
    using base::base;
    virtual void generate() override;
  private:
    void carve_from(maze_util::cell&);
  };

  /* Recursive division generator */
  struct re_div : public base {
    using base::base;
    virtual void generate() override;

  private:
    std::stack<field> pr_fields;
  };

  /* Prim's generator */
  struct prims : public base {
    using base::base;
    virtual void generate() override;
 
  private:
    std::vector<maze_util::cell> pr_adjacent;

    void mark_cell(const unsigned, const unsigned);
    void add_adjacent(const unsigned&, const unsigned&);
    std::vector<wall> get_neigbors(const unsigned&, const unsigned&);
  };

  /* Kruskal's generator */
  struct kruskals : public base {
    using base::base;
    ~kruskals();
    virtual void generate() override;
    
  private:
    std::vector<wall> pr_walls;
    std::vector<avl_tree<unsigned>*> pr_cells;
  };
}

